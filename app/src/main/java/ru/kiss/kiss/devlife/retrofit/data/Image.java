package ru.kiss.kiss.devlife.retrofit.data;

public class Image {
    private String gifURL;
    private String description;

    public Image(String url, String description){
        this.gifURL = url;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return gifURL;
    }
}
