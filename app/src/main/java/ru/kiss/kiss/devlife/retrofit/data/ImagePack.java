package ru.kiss.kiss.devlife.retrofit.data;

import java.util.List;

public class ImagePack {
    private List<Image> result;
    private Integer totalCount; // 12922

    public ImagePack(List<Image> result, Integer totalCount){
        this.result = result;
        this.totalCount = totalCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public List<Image> getResult() {
        return result;
    }
}
