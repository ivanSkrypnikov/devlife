package ru.kiss.kiss.devlife.activity;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.Gravity;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kiss.kiss.devlife.R;
import ru.kiss.kiss.devlife.retrofit.NetworkService;
import ru.kiss.kiss.devlife.retrofit.data.Image;
import ru.kiss.kiss.devlife.retrofit.data.ImagePack;

public class MainActivity extends AppCompatActivity {

    public static final String TOP = "top";
    public static final String HOT = "hot";
    public static final String LATEST = "latest";
    private static String tabName;
    Map<String, Integer> tabToPage;
    Map<String, Iterator<Image>> tabToIterator;
    Map<String, Stack<Image>> tabToPrevImages;
    ImageView imageView;
    TextView imageTitle;
    Image currentImage;
    Button nextButton;
    Button prevButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        setHandlers();

        NetworkService
                .getInstance()
                .getRequestAPI()
                .getMemes(tabName,tabToPage.get(tabName))
                .enqueue(getNetworkCallBack());
    }

    private Callback<ImagePack> getNetworkCallBack(){
        return new Callback<ImagePack>() {
            @Override
            public void onResponse(Call<ImagePack> call, Response<ImagePack> response) {
                List<Image> result = response.body().getResult();
                Iterator<Image> imageIterator = result.iterator();
                if (imageIterator.hasNext()){
                    if (currentImage != null){
                        tabToPrevImages.get(tabName).push(currentImage);
                        prevButton.setEnabled(true);
                    }
                    currentImage = imageIterator.next();
                    tabToIterator.put(tabName,imageIterator);
                    Glide
                            .with(getApplicationContext())
                            .load(currentImage.getUrl())
                            .into(imageView);
                    imageTitle.setText(currentImage.getDescription());
                    tabToPage.put(tabName, tabToPage.get(tabName) + 1);
                } else
                    showToast("Sorry, but the response from the server was empty!");
            }

            @Override
            public void onFailure(Call<ImagePack> call, Throwable t) {
                showToast("Problem with your Internet connection. Please, try again!");
                t.printStackTrace();
            }
        };
    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void initialize(){
        tabToIterator = new HashMap<>();
        tabToPrevImages = new HashMap<>();
        tabToPrevImages.put(TOP, new Stack<>());
        tabToPrevImages.put(HOT, new Stack<>());
        tabToPrevImages.put(LATEST, new Stack<>());
        tabToPage = new HashMap<>();
        tabToPage.put(TOP,0);
        tabToPage.put(HOT,0);
        tabToPage.put(LATEST,0);
        tabName = getResources().getString(R.string.latest);
        imageView = findViewById(R.id.imageView);
        imageTitle = findViewById(R.id.imageTitle);

        nextButton = findViewById(R.id.buttonNext);
        prevButton = findViewById(R.id.buttonPrev);
        prevButton.setEnabled(false);
    }

    private void setHandlers(){
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabName = tab.getText().toString();
                if (tabToPrevImages.get(tabName).empty())
                    NetworkService
                            .getInstance()
                            .getRequestAPI()
                            .getMemes(tabName,tabToPage.get(tabName))
                            .enqueue(getNetworkCallBack());
                else {
                    currentImage = tabToPrevImages.get(tabName).pop();
                    Glide
                            .with(getApplicationContext())
                            .load(currentImage.getUrl())
                            .into(imageView);
                    imageTitle.setText(currentImage.getDescription());
                }
                if (tabToPrevImages.get(tabName).empty())
                    prevButton.setEnabled(false);
                else
                    prevButton.setEnabled(true);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (currentImage != null)
                    tabToPrevImages.get(tab.getText().toString())
                            .push(currentImage);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tabToPrevImages.get(tabName).empty()){
                    currentImage = tabToPrevImages.get(tabName).pop();
                    Glide
                            .with(getApplicationContext())
                            .load(currentImage.getUrl())
                            .into(imageView);
                    imageTitle.setText(currentImage.getDescription());
                    if (tabToPrevImages.get(tabName).empty())
                        prevButton.setEnabled(false);
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tabToIterator.containsKey(tabName) && tabToIterator.get(tabName).hasNext()){
                    tabToPrevImages.get(tabName).push(currentImage);
                    currentImage = tabToIterator.get(tabName).next();
                    Glide
                            .with(getApplicationContext())
                            .load(currentImage.getUrl())
                            .into(imageView);
                    imageTitle.setText(currentImage.getDescription());
                    prevButton.setEnabled(true);
                }
                else
                    NetworkService
                            .getInstance()
                            .getRequestAPI()
                            .getMemes(tabName,tabToPage.get(tabName))
                            .enqueue(getNetworkCallBack());
            }
        });

    }

}