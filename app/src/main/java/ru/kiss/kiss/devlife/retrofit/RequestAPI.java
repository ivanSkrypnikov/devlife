package ru.kiss.kiss.devlife.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.kiss.kiss.devlife.retrofit.data.Image;
import ru.kiss.kiss.devlife.retrofit.data.ImagePack;

public interface RequestAPI {

    @GET("/{type}/{page}?json=true")
    public Call<ImagePack> getMemes(@Path("type") String type, @Path("page") int page);

}
